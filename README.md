# BearPi-GD32F303RGT6

#### 介绍
小熊派GD32学习过程中的资料和编写的代码



#### 使用说明

1.  提交代码
```
touch README.md
git add README.md
git commit -m "first commit"
git push -u origin master
```


2.  同步仓库:
```bash
git pull origin master
```
