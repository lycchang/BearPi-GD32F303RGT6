#include "main.h"
#include "led.h"
#include "uart.h"

int main(void)
{
	int t = 0;
	/* 配置系统时钟 */
	systick_config();
	/* 初始化LED */
	led_init();
	/* 初始化USART0 */
	uart0_init(115200);
	
	/* 通过串口打印 Hello world! */
	u1_printf("Hello world! ");
	u1_printf("I am William. \r\n");

    while(1)
	{
		if(UART0_RX_STAT > 0)
		{
			UART0_RX_STAT = 0;
			u1_printf("RECEIVE %d data:%s \r\n", UART0_RX_NUM, UART0_RX_BUF);
		}
		
		delay_1ms(10);
		t++;
		if(t % 200 == 0) LED(0);		/* turn off LED */
		else if(t % 200 == 100) LED(1);	/* turn on LED */
        
	}
}

