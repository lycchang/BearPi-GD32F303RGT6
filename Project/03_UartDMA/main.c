#include "main.h"
#include "led.h"
#include "uart.h"

int main(void)
{
	/* 配置系统时钟 */
	systick_config();
	/* 初始化LED */
	led_init();
	/* 初始化USART0 */
	uart0_init(115200);

    while(1)
	{
		/* 通过串口打印 Hello world! */
		u1_printf("Hello world! ");
		u1_printf("I am William. \r\n");
		
        /* turn on LED */
        LED(1);
        delay_1ms(500);

        /* turn off LED */
        LED(0);
        delay_1ms(500);
    }
}

