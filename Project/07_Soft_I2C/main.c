#include "main.h"
#include "led.h"
#include "uart.h"
#include "at24c02.h"

int main(void)
{
	uint8_t buff;
	uint8_t err;
	/* 配置系统时钟 */
	systick_config();
	/* 初始化LED */
	// led_init();
	/* 初始化USART0 */
	uart0_init(115200);
	/* 初始化AT24C02 */
	at24c02_init();
	
	/* 通过串口打印 Hello world! */
	u0_printf("Hello world! ");
	u0_printf("I am William. \r\n");
	
	err = AT24C02_Write_Byte(0x0a, 0xa5);
	if(err == 0)
		printf("Write 0xa5 to addr 0x0a ok \r\n");
	else
	{
		printf("Write 0xa5 to addr 0x0a err \r\n");
		printf("err num : 0x%x \r\n",err);
	}
	
	if(AT24C02_Read_Byte(0x0a, &buff) == 0)
		printf("Read data: 0x%x from addr 0x0a ok \r\n", buff);
	else
		printf("Read data from addr 0x0a err \r\n");
	
    while(1)
	{
		if(UART0_RX_STAT > 0)
		{
			UART0_RX_STAT = 0;
			u0_printf("RECEIVE %d data:%s \r\n", UART0_RX_NUM, UART0_RX_BUF);
		}
		
		delay_1ms(10);
		
	}
}

