/* LED的硬件初始化代码 */
#include "led.h"

/* LED硬件初始化 */
void led_init(void)
{
	/* 使能LED所使用的GPIOB时钟 */
    rcu_periph_clock_enable(RCU_GPIOB);

    /* 配置LED使用的引脚为推挽输出 */ 
    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0);
    /* reset LED GPIO pin */
    gpio_bit_reset(GPIOB, GPIO_PIN_0);
}



