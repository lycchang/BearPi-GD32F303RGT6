#ifndef __LED_H
#define __LED_H

#include "main.h"

/* 宏定义LED的控制方式 */
#define LED(n) (n?gpio_bit_set(GPIOB, GPIO_PIN_0):gpio_bit_reset(GPIOB, GPIO_PIN_0))

/* LED硬件初始化 */
void led_init(void);

#endif

